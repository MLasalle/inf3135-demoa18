#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#define FILE_NB_LINE 231

int compare(const void* a, const void* b) {
    return strcmp(*(const char**) a, *(const char**) b);
}

int main(int argc, char **argv) {
	FILE *fp = fopen("countries.txt" , "r");
   	if (fp == NULL) {
		printf("Error opening file");
		exit(EXIT_FAILURE);
	}

	char *capitals[FILE_NB_LINE];
	char *capital;
	size_t l = 0;
	int length;
	char *line = NULL;
	int count = 0;


	while ((length = getline(&line, &l, fp)) != -1) {
		capital = strtok(line,",");
		capital = strtok(NULL,",");

                capitals[count] = (char *)malloc(sizeof(char)*strlen(capital));

		strcpy(capitals[count], capital);
		++count;
	}

	free(line);
	fclose(fp);

#ifdef DEBUG

	printf("****** Before sort *******\n");

	for (int i = 0; i < count; i++)
		printf("%s",capitals[i]);

	printf("***************************\n\n");

#endif

	qsort(capitals, count, sizeof(char*), compare);

#ifdef DEBUG

        printf("****** After sort *******\n");

        for (int i = 0; i < count; i++)
                printf("%s",capitals[i]);

        printf("***************************\n\n");

#endif

	for (int i = 0; i < count; i++)
                free(capitals[i]);

	return 0;
}
