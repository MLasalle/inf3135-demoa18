# Solution laboratoire 9 | 20 novembre 2018

[Énoncé du laboratoire](https://gitlab.com/Morriar/inf3135-laboratoires/blob/master/semaine09.md)

## Utilisation

```shell
make clean
make
./sort
./search
./treemap
```

Pour version debug
```shell
make clean
make DEBUG=1
./sort
./search
./treemap
```

## Ressources
[Trouver une fuite de mémoire avec valgrind](https://www.cprogramming.com/debugging/valgrind.html)
