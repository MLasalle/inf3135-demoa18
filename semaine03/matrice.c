#include <stdio.h>

#define LIGNES 3
#define COLONNES 3


void afficherMatrice(int matrice[LIGNES][COLONNES]);
void additionMatrice(int matrice1[LIGNES][COLONNES], int matrice2[LIGNES][COLONNES], int matriceTotal[LIGNES][COLONNES]);

int main(int argc, char const *argv[]) {
	int matrice1[LIGNES][COLONNES] = {{1,2,3},{4,5,6},{7,8,9}};
	int matrice2[LIGNES][COLONNES] = {{1,1,1},{1,1,1},{1,1,1}};
	int matriceTotal[LIGNES][COLONNES];

	additionMatrice(matrice1, matrice2, matriceTotal);

	afficherMatrice(matrice1);
	printf("%s\n", "+");
	afficherMatrice(matrice2);
	printf("%s\n", "=");
	afficherMatrice(matriceTotal);

	return 0;
}

void afficherMatrice(int matrice[LIGNES][COLONNES]){
	int i;
	int j;
	for (i = 0; i < LIGNES; i++) {
		for (j = 0; j < COLONNES; j++) {
			printf("%d ", matrice[i][j]);
		}
		printf("\n");
	}
}

void additionMatrice(int matrice1[LIGNES][COLONNES], int matrice2[LIGNES][COLONNES], int matriceTotal[LIGNES][COLONNES]){
	int i;
	int j;
	for (i = 0; i < LIGNES; i++) {
		for (j = 0; j < COLONNES; j++) {
			matriceTotal[i][j] = matrice1[i][j]+matrice2[i][j];
		}
	}
}
