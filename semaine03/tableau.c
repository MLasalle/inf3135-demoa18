#include <stdio.h>

#define TAILLE_TAB 5

const double *trouverElement(const double tableau[], unsigned int taille, double element);

int main(int argc, char const *argv[]) {
	double tab[TAILLE_TAB] = {9.0, 8.3, 7.2, 6.8, 10.65};
	printf("%s%p\n", "L'adresse de 6.8 est : ", &tab[3]);

	const double *adresseTrouve = trouverElement(tab, TAILLE_TAB, 6.8);

	printf("%s%p\n", "L'adresse trouve est : ", adresseTrouve);
	return 0;
}

const double *trouverElement(const double tableau[], unsigned int taille, double element){
	int i;
	for (i = 0; i < taille; i++) {
		if (tableau[i] == element){
			return &tableau[i];
		}
	}
	return NULL;
}
