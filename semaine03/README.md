# Solution laboratoire 3 | 2 octobre 2018

[Énoncé du laboratoire](https://gitlab.com/Morriar/inf3135-laboratoires/blob/master/semaine03.md)

## Utilisation
```shell
$ make clean
$ make
$ ./reverse alpha beta gamma
$ ./palindrome
$ ./tableau
$ ./matrice
```
