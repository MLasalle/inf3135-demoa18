#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

// Types

struct QueueNode {
	char content;           // Contenu du noeud
	struct QueueNode *prev; // Noeud precedent
	struct QueueNode *next; // Noeud suivant
};

typedef struct {
	struct QueueNode *first; // Pointeur vers le premier noeud
	struct QueueNode *last;  // Pointeur vers le dernier noeud
} Queue;

// Prototypes

Queue queueCreate();
bool queueIsEmpty(const Queue *s);
void queuePush(Queue *s, char content);
char queuePop(Queue *s);
void queueDelete(Queue *s);

int main(int argc, char const *argv[])
{
	Queue q = queueCreate();

	if (queueIsEmpty(&q)) {
		printf("Queue is empty\n");
	} else {
		printf("Queue is not empty\n");
	}

	queuePush(&q, 'x');
        queuePush(&q, 'y');
        queuePush(&q, 'z');

	printf("%c\n",queuePop(&q));
	printf("%c\n",queuePop(&q));

        queuePush(&q, 'a');
        queuePush(&q, 'b');

        printf("%c\n",queuePop(&q));
        printf("%c\n",queuePop(&q));
        printf("%c\n",queuePop(&q));

	queueDelete(&q);

	return 0;
}

void queueDelete(Queue *s) {
        while(!queueIsEmpty(s)) queuePop(s);
}

Queue queueCreate() {
	Queue q;
	q.first = NULL;
	q.last = NULL;
	return q;
}

bool queueIsEmpty(const Queue *s) {
	return s->last == NULL ? true : false;
}

void queuePush(Queue *s, char content) {
	struct QueueNode *qn;

	qn = malloc(sizeof(struct QueueNode));
	qn->content = content;
        qn->next = NULL;

	if(queueIsEmpty(s)) {
		s->first = qn;
		s->last = qn;
	} else {
		s->last->next = qn;
		s->last = qn;
	}
}

char queuePop(Queue *s) {

	if (queueIsEmpty(s))
		return '\0';

	char content = s->first->content;
	struct QueueNode *tmp = s->first;

	s->first = s->first->next;

	free(tmp);

	if (s->first == NULL) s->last = NULL;

	return content;
}

