#include <stdio.h>

int main(int argc, char const *argv[]) {
	printf("digraph {\n");

	for (int i = 1;i <= 48; ++i) {
		for (int j = 1; j <= 48; ++j) {
			if (j % i == 0) {
				printf("%d -> %d;\n",i,j);
			}
		}
	}

	printf("}");

	return 0;
}
