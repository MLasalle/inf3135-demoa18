#include <stdio.h>
#include <stdbool.h>

bool esttri(int tab[], int s);

int main(int argc, char *argv[]) {
	int tableau[] = {1,1,2,3,4,5};
	int tableau2[] = {10,1,12,3,4,5,1};


	if (esttri(tableau, 6)) {
		printf("Le tableau est en ordre croissant.\n");
	} else {
		printf("Le tableau n'est pas en ordre croissant.\n");
	}

        if (esttri(tableau2, 7)) {
                printf("Le tableau est en ordre croissant.\n");
        } else {
                printf("Le tableau n'est pas en ordre croissant.\n");
        }

        return 0;
}

bool esttri(int tab[], int s) {
	for (int i = 0; i < s-1; ++i) {
		if (tab[i] > tab[i+1])
			return false;
	}
	return true;
}


