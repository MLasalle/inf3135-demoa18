#include <stdio.h>

int main(int argc, char *argv[]) {

	int tabint[] = {1,2,3};
	char tabchar[] = "xxx";
	char tabchar2[] = {'x','x','x'};


	int size = sizeof(tabint)/sizeof(tabint[0]);

	printf("Size tabint: %d\n", size);

	size = sizeof(tabchar)/sizeof(tabchar[0]);

	printf("Size tabchar: %d\n", size);

        size = sizeof(tabchar2)/sizeof(tabchar2[0]);

        printf("Size tabchar2: %d\n", size);

	return 0;
}


