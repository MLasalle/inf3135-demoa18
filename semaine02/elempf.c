#include <stdio.h>

int elementPlusFrequent(int tab[], int s);

int main(int argc, char *argv[]) {
        int tableau[] = {3,1,3,2,1,1,3,2,3,2};
	int tableau2[] = {10,10,10,1,1};


	printf("Element le plus frequent dans le tableau: %d résultat attendu 3\n"
		, elementPlusFrequent(tableau, 10));

        printf("Element le plus frequent dans le tableau2: %d résultat attendu 10\n"
                , elementPlusFrequent(tableau2, 5));

        return 0;
}

int elementPlusFrequent(int tab[], int s) {
	int frequenceMax = 0;
	int frequence = 1;
	int element;
        for (int i = 0; i < s-1; ++i) {
		for(int j = i + 1; j < s; ++j) {
                	if (tab[i] == tab[j]) {
				++frequence;
			}
		}
		if (frequence > frequenceMax) {
			frequenceMax = frequence;
			element = tab[i];
		}
		frequence = 1;
        }
        return element;
}


