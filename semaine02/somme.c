#include <stdio.h>

int somme(int tab[], int s);

int main(int argc, char *argv[]) {
	int resultat;
	int tab[] = {1,1,1,1,1};
	int tab2[] = {-10,0,10};

	resultat = somme(tab,5);

	printf("Résultat: %d, résultat attendu 5\n",resultat);

	resultat = somme(tab2,3);

	printf("Résultat: %d, résultat attendu 0\n",resultat);

        return 0;
}

int somme(int tab[], int s) {
	int r = 0;
	for(int i = 0; i < s; ++i) {
		r += tab[i];
	}
	return r;
}


