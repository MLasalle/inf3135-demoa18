#include <stdio.h>

int nombreOccurrences(char r, char tab[], int l);

int main(int argc, char *argv[]) {
        char chaine[] = "testtesttest";
	char recherche = 't';
	int resultat;

	resultat = nombreOccurrences(recherche, chaine, 12);

        printf("Recherche du nombre de '%c' dans la chaine '%s' resultat: %d\n"
		, recherche, chaine, resultat);

	recherche = 'e';
	resultat = nombreOccurrences(recherche, chaine, 12);

        printf("Recherche du nombre de '%c' dans la chaine '%s' resultat: %d\n"
                , recherche, chaine, resultat);

        return 0;
}

int nombreOccurrences(char r, char tab[], int l) {
        int nb = 0;
	for (int i = 0; i < l; ++i) {
		if (tab[i] == r)
			++nb;
	}
        return nb;
}


