#include "stdio.h"
#include "math.h"

enum TypeNombre {
	INT, FLOAT, DOUBLE
};

typedef struct {           // Un nombre
	enum TypeNombre type;  // Le type de nombre
	union {
		int i;
		float f;
		double d;
	} valeur;              // La valeur
} Nombre;

Nombre max(Nombre a, Nombre b);
void afficherNombre(Nombre nombre);

int main(int argc, char const *argv[])
{
	Nombre a;
	Nombre b;

	a.type = INT;
	a.valeur.i = 20;

	b.type = DOUBLE;
	b.valeur.d = 15.6;

	printf("Le plus grand nombre entre ");
	afficherNombre(a);
	printf(" et ");
	afficherNombre(b);
	printf(" Resultat: ");

	Nombre plusGrand = max(a,b);

	afficherNombre(plusGrand);

	printf("\n");

	return 0;
}

Nombre max(Nombre a, Nombre b){
	double aValeur;
	double bValeur;
	switch (a.type) {
		case INT:
			aValeur = (double) a.valeur.i;
			break;
		case FLOAT:
			aValeur = (double) a.valeur.f;
			break;
		case DOUBLE:
			aValeur = (double) a.valeur.d;
			break;
		default:
			break;
	}
	switch (b.type) {
		case INT:
			bValeur = (double) b.valeur.i;
			break;
		case FLOAT:
			bValeur = (double) b.valeur.f;
			break;
		case DOUBLE:
			bValeur = (double) b.valeur.d;
			break;
		default:
			break;
	}

	if(aValeur > bValeur) return a;

	return b;
}

void afficherNombre(Nombre nombre) {
	int i;
	float f;
	double d;
        switch (nombre.type) {
                case INT:
                        i = (int) nombre.valeur.i;
			printf("int %d", i);
                        break;
                case FLOAT:
			f = (float) nombre.valeur.f;
			printf("float %f", f);
                        break;
                case DOUBLE:
			d = (double) nombre.valeur.d;
			printf("double %f", d);
                        break;
                default:
                        break;
        }
}
