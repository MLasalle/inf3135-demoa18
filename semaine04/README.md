# Solution laboratoire 4 | 9 octobre 2018

[Énoncé du laboratoire](https://gitlab.com/Morriar/inf3135-laboratoires/blob/master/semaine04.md)

## Utilisation
```shell
$ make clean
$ make
$ ./structures
$ ./union
$ ./enum
```

## Ressources
[La distance entre deux points](http://www.alloprof.qc.ca/BV/pages/m1311.aspx)

[Tutoriel gdb](http://www.unknownroad.com/rtfm/gdbtut/)
