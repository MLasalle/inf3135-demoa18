#include <stdio.h>
#include <math.h>

struct Point { // Un point en 2D
    double x;  // Sa coordonnée x
    double y;  // Sa coordonnée y
};

struct Segment {
    struct Point p1;
    struct Point p2;
};

struct Triangle {
    struct Point points[3];
};

void initialiserSegment(struct Segment *segment, double x1, double y1, double x2, double y2);
void initialiserSegment2(struct Segment *segment, double x1, double y1, double x2, double y2);
void initialiserSegment3(struct Segment *segment, double x1, double y1, double x2, double y2);
double longueurSegment(const struct Segment *segment);
void initialiserTriangle(struct Triangle *triangle, double x1, double y1, double x2, double y2, double x3, double y3);
double perimetreTriangle(const struct Triangle *triangle);


int main(int argc, char const *argv[]) {
	struct Segment segment;
	struct Triangle triangle;

	initialiserSegment(&segment, 0.0, 1.0, 1.0, 0.0);
	// initialiserSegment2(&segment, 1.0, 2.0, 3.0, 4.0);
	// initialiserSegment3(&segment, 1.0, 2.0, 3.0, 4.0);
	printf("Segment point 1 => (%.2f,%.2f)  | point 2 => (%.2f,%.2f)\n", segment.p1.x, segment.p1.y, segment.p2.x, segment.p2.y);

	printf("Longueur du segment : %.2f\n\n", longueurSegment(&segment));

	initialiserTriangle(&triangle, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0);

	printf("Triangle point 1 => (%.2f,%.2f)  | point 2 => (%.2f,%.2f) | point 2 => (%.2f,%.2f)\n",
		triangle.points[0].x, triangle.points[0].y,
		triangle.points[1].x, triangle.points[1].y,
		triangle.points[2].x, triangle.points[2].y);

	printf("Perimetre triangle: %.2f\n", perimetreTriangle(&triangle));

	return 0;
}

void initialiserSegment(struct Segment *segment, double x1, double y1, double x2, double y2) {
	segment->p1.x = x1;
	segment->p1.y = y1;
	segment->p2.x = x2;
	segment->p2.y = y2;
}

void initialiserSegment2(struct Segment *segment, double x1, double y1, double x2, double y2) {
	struct Point p1 = {x1, y1};
	struct Point p2 = {x2, y2};
	*segment = (struct Segment) {p1, p2};
}

void initialiserSegment3(struct Segment *segment, double x1, double y1, double x2, double y2) {
	struct Point p1 = {x1, y1};
	struct Point p2 = {x2, y2};
	(*segment).p1 = p1;
	(*segment).p2 = p2;
}

double longueurSegment(const struct Segment *segment) {
	double a = segment->p2.x - segment->p1.x;
	double b = segment->p2.y - segment->p1.y;
	return sqrt(a*a+b*b);
}

void initialiserTriangle(struct Triangle *triangle, double x1, double y1, double x2, double y2, double x3, double y3) {
	triangle->points[0].x = x1;
	triangle->points[0].y = y1;
	triangle->points[1].x = x2;
	triangle->points[1].y = y2;
	triangle->points[2].x = x3;
	triangle->points[2].y = y3;
}

double perimetreTriangle(const struct Triangle *triangle) {
	struct Segment segment1,segment2,segment3;

	initialiserSegment(&segment1, triangle->points[0].x, triangle->points[0].y,
		triangle->points[1].x, triangle->points[1].y);
        initialiserSegment(&segment2, triangle->points[0].x, triangle->points[0].y,
                triangle->points[2].x, triangle->points[2].y);
        initialiserSegment(&segment3, triangle->points[1].x, triangle->points[1].y,
                triangle->points[2].x, triangle->points[2].y);

	return longueurSegment(&segment1)
		+ longueurSegment(&segment2)
		+ longueurSegment(&segment3);
}
