# Solution laboratoire 8 | 13 novembre 2018

[Énoncé du laboratoire](https://gitlab.com/Morriar/inf3135-laboratoires/blob/master/semaine08.md)

## Utilisation

```shell
make clean
make
./country
```

## Commandes

Pour obtenir le fichier capitales.txt.

N'oubliez pas de modifier le fichier pour éliminer les lignes vides et les noms avec caractères spéciaux.

```shell
$ grep capital countries.json | sed -e 's/^.*: \["//' | sed -e 's/"],//' > capitales.txt
```

Pour obtenir le fichier countries.txt.

N'oubliez pas d'enlever les lignes incomplètes à la fin du fichier.

```shell
paste -d "," <(grep cca3 countries.json | sed -e 's/^.*: "//' | sed -e 's/",//') capitales.txt > countries.txt
```

## Resources

Fichier [countries.json](https://raw.githubusercontent.com/mledoze/countries/master/countries.json)

Exemple [jansson](https://raw.githubusercontent.com/akheron/jansson/master/examples/simple_parse.c)

voir les fonctions:
* print_json_object
* load_json
* main
