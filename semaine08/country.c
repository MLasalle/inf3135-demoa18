#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "treemap.h"

#define BUFFER_LEN 500

int main(int argc, char const *argv[]) {
	char buffer[BUFFER_LEN];
	char *saveptr;
	char *cca3, *capital;
	TreeMap map = treemapCreate();

	FILE *fp = fopen("countries.txt" , "r");
   	if (fp == NULL) {
		printf("Error opening file");
		exit(EXIT_FAILURE);
	}


	while (fgets(buffer, BUFFER_LEN, fp) != NULL) {
		cca3 = strtok_r(buffer, ",", &saveptr);
		capital = strtok_r(NULL, "\n", &saveptr);
		treemapSet(&map, cca3, capital);
	}

  	treemapPrint(&map);

  	treemapDelete(&map);
}

