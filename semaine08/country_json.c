#include <stdio.h>
#include <stdlib.h>
#include <jansson.h>
//#include "treemap.h"

int main() {
	FILE *fp = fopen("countries.json" , "r");
   	if (fp == NULL) {
		printf("Error opening file");
		exit(EXIT_FAILURE);
	}


	json_t* root = json_loadf(fp, 0, NULL);

	if (!json_is_array(root)) {
		exit(EXIT_FAILURE);
	}

	size_t index;
	json_t *value;
	json_t *capital_j;
	json_t *cca3_j;
	const char *cca3;
	const char *capital;

	json_array_foreach (root, index, value) {
    		cca3_j = json_object_get(value, "cca3");

		capital_j = json_array_get(json_object_get(value, "capital"),0);

		cca3 = json_string_value(cca3_j);
		capital = json_string_value(capital_j);

    		printf("%s,%s\n", cca3, capital);
	}

	return 0;
}

