#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "treemap.h"

// Types
// -----

struct TreeNode {
    char *key;              // Cle du noeud
    char *value;            // Valeur associee
    struct TreeNode *left;  // Fils gauche
    struct TreeNode *right; // Fils droit
};


// Prototypes
// ----------

void treemapInsertNode(struct TreeNode **node, char *key, char *value);
void treemapPrintRecursive(const struct TreeNode *node);
void treemapDeleteRecursive(struct TreeNode *node);

// Implementation
// --------------

TreeMap treemapCreate() {
    TreeMap t = {NULL};
    return t;
}

char *treemapGet(const TreeMap *t, char *key) {
    struct TreeNode *node = treemapGetNode(t->root, key);
    if (node == NULL)
        return NULL;
    else
        return node->value;
}

void treemapSet(TreeMap *t, char *key, char *value) {
    struct TreeNode *node = treemapGetNode(t->root, key);
    if (node != NULL) {
        free(node->value);
        node->value = strdup(value);
    } else {
        treemapInsertNode(&(t->root), key, value);
    }
}

/**
 * Insere un noeud dans l'arbre representant une table associative.
 *
 * Note : cette fonction devrait etre privee (lorsqu'on verra les
 * modules, elle ne doit pas etre dans l'interface).
 *
 * @param node   Le noeud courant
 * @param key    La cle a inserer
 * @param value  La valeur associee
 */
void treemapInsertNode(struct TreeNode **node, char *key, char *value) {
    if (*node == NULL) {
        *node = malloc(sizeof(struct TreeNode));
        (*node)->key = strdup(key);
        (*node)->value = strdup(value);
        (*node)->left = NULL;
        (*node)->right = NULL;
    } else if (strcmp(key, (*node)->key) < 0) {
        treemapInsertNode(&(*node)->left, key, value);
    } else {
        treemapInsertNode(&(*node)->right, key, value);
    }
}

bool treemapHasKey(const TreeMap *t, char *key) {
    return treemapGetNode(t->root, key) != NULL;
}

struct TreeNode *treemapGetNode(const struct TreeNode *node, char *key) {
    if (node == NULL) {
        return NULL;
    } else {
        int cmp = strcmp(key, node->key);
        if (cmp == 0)
            return (struct TreeNode*)node;
        else if (cmp < 0)
            return treemapGetNode(node->left, key);
        else
            return treemapGetNode(node->right, key);
    }
}

void treemapPrint(const TreeMap *t) {
    printf("TreeMap {\n");
    treemapPrintRecursive(t->root);
    printf("}\n");
}

/**
 * Affiche en ordre infixe les paires (key, value) presentes
 * dans l'arbre.
 *
 * Note : cette fonction devrait etre privee (lorsqu'on verra les
 * modules, elle ne doit pas etre dans l'interface).
 */
void treemapPrintRecursive(const struct TreeNode *node) {
    if (node != NULL) {
        treemapPrintRecursive(node->left);
        printf("  %s: %s\n", node->key, node->value);
        treemapPrintRecursive(node->right);
    }
}

void treemapDelete(TreeMap *t) {
    treemapDeleteRecursive(t->root);
}

/**
 * Libere recursivement l'espace memoire utilise par les noeuds
 * de l'arbre representant une table associative.
 *
 * @param node  Le noeud courant
 *
 * Note : cette fonction devrait etre privee (lorsqu'on verra les
 * modules, elle ne doit pas etre dans l'interface).
 */
void treemapDeleteRecursive(struct TreeNode *node) {
    if (node != NULL) {
        treemapDeleteRecursive(node->left);
        treemapDeleteRecursive(node->right);
        free(node->key);
        free(node->value);
        free(node);
    }
}
