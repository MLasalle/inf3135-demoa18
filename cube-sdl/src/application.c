/**
 * Implement services provided by application module.
 *
 * @origin https://bitbucket.org/ablondin-projects/maze-sdl
 *
 */
#include "application.h"
#include "sdl2.h"
#include <stdio.h>

struct application *application_initialize()
{
        struct application *app;
        if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
                fprintf(stderr, "SDL failed to initialize: %s\n", SDL_GetError());
                return NULL;
        }
        if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
                fprintf(stderr, "Warning: Linear texture filtering not enabled!");
        }
        app = (struct application*)malloc(sizeof(struct application));
        app->window = SDL_CreateWindow("Application",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
        if (app->window == NULL) {
                fprintf(stderr, "Window could not be created: %s\n", SDL_GetError());
                return NULL;
        } else {
                app->surface = SDL_GetWindowSurface(app->window);
                SDL_FillRect(app->surface, NULL, SDL_MapRGB(app->surface->format, 0xFF, 0xFF, 0xFF));
                SDL_UpdateWindowSurface(app->window);
        }
        app->renderer = SDL_CreateRenderer(app->window, -1,
        SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    	if (app->renderer == NULL) {
        	fprintf(stderr, "Renderer could not be created: %s\n", SDL_GetError());
        	return NULL;
    	}

        app->state = APPLICATION_STATE_PLAY;
	app->gme = NULL;

        return app;
}

void application_run(struct application *app)
{
        while (app->state != APPLICATION_STATE_QUIT) {
                SDL_Event e;
                SDL_PollEvent(&e);
		if (app->state == APPLICATION_STATE_PLAY) {
			app->gme = game_initialize(app->renderer);
			if (app->gme == NULL) {
                                fprintf(stderr, "Failed to initialize game:\n");
                                app->state = APPLICATION_STATE_QUIT;
                        } else {
                                game_run(app->gme);
                                if (app->gme->state == GAME_QUIT) {
                                        app->state = APPLICATION_STATE_QUIT;
                                }
                        }
                }

                if(e.type == SDL_WINDOWEVENT
                                && e.window.event == SDL_WINDOWEVENT_CLOSE)
                        app->state = APPLICATION_STATE_QUIT;
        }
}

void application_shut_down(struct application *app)
{
        SDL_DestroyRenderer(app->renderer);
        SDL_DestroyWindow(app->window);
        game_delete(app->gme);
	free(app);
        app = NULL;
        SDL_Quit();
}
