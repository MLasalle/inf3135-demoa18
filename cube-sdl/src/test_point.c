/**
 * File test_application.c
 *
 * CUnit tests for application.c
 *
 */
#include "point.h"
#include <CUnit/Basic.h>

void test_point_set_to_zero()
{
        struct point position;
        point_set_to_zero(&position);
        CU_ASSERT(position.x == 0 && position.y == 0);
}

void test_point_add_x()
{
        struct point position, speed;
        position.x = 5;
        position.y = 5;
        speed.x = 10;
        speed.y = 0;
        point_add(&position, &speed);
        CU_ASSERT(position.x == 15 && position.y == 5);
}

void test_point_add_y()
{
        struct point position, speed;
        position.x = 5;
        position.y = 5;
        speed.x = 0;
        speed.y = 10;
        point_add(&position, &speed);
        CU_ASSERT(position.x == 5 && position.y == 15);
}

void test_point_add_x_y()
{
        struct point position, speed;
        position.x = 5;
        position.y = 5;
        speed.x = 15;
        speed.y = 20;
        point_add(&position, &speed);
        CU_ASSERT(position.x == 20 && position.y == 25);
}

void test_point_remove_x()
{
        struct point position, speed;
        position.x = 20;
        position.y = 10;
        speed.x = 5;
        speed.y = 0;
        point_subtract(&position, &speed);
        CU_ASSERT(position.x == 15 && position.y == 10);
}

void test_point_remove_y()
{
        struct point position, speed;
        position.x = 20;
        position.y = 10;
        speed.x = 0;
        speed.y = 5;
        point_subtract(&position, &speed);
        CU_ASSERT(position.x == 20 && position.y == 5);
}

void test_point_remove_x_y()
{
        struct point position, speed;
        position.x = 50;
        position.y = 50;
        speed.x = 15;
        speed.y = 20;
        point_subtract(&position, &speed);
        CU_ASSERT(position.x == 35 && position.y == 30);
}

void test_point_set_to()
{
        struct point position;
        int x, y;
        position.x = 20;
        position.y = 20;
        x = 5;
        y = 10;
        point_set_to(&position, x, y);
        CU_ASSERT(position.x == x && position.y == y);

}

int main()
{
	CU_pSuite pSuite = NULL;
	if (CU_initialize_registry() != CUE_SUCCESS)
		return CU_get_error();

	pSuite = CU_add_suite("Test point manipulation", NULL, NULL);
	if (pSuite == NULL) {
		CU_cleanup_registry();
		return CU_get_error();
	}

	if (CU_add_test(pSuite, "point set to 0",
                test_point_set_to_zero) == NULL
        || CU_add_test(pSuite, "point add speed x",
                test_point_add_x) == NULL
        || CU_add_test(pSuite, "point add speed y",
                test_point_add_y) == NULL
        || CU_add_test(pSuite, "point add speed x and y",
                test_point_add_x_y) == NULL
        || CU_add_test(pSuite, "point remove speed x",
                test_point_add_x) == NULL
        || CU_add_test(pSuite, "point remove speed y",
                test_point_add_y) == NULL
        || CU_add_test(pSuite, "point remove speed x and y",
                test_point_add_x_y) == NULL
        || CU_add_test(pSuite, "point set to",
                test_point_set_to) == NULL) {
		CU_cleanup_registry();
		return CU_get_error();
	}

	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_cleanup_registry();
	return CU_get_error();
}
