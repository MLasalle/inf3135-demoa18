#ifndef GAME_H
#define GAME_H

#include "sdl2.h"

// --------------- //
// Data structures //
// --------------- //

enum game_state {
	GAME_PLAY, // The player is playing
	GAME_QUIT, // The player is quitting
};

struct game {
        SDL_Renderer *renderer;      // The renderer
	enum game_state state;       // The state of the game
};

// --------- //
// Functions //
// --------- //

/**
 * Creates a new game.
 *
 * @param   The renderer for the game
 * @return  A pointer to the game, NULL if there was an error
 */
struct game *game_initialize(SDL_Renderer *renderer);

/**
 * Delete the game.
 *
 * @param game  The game to delete
 */
void game_delete(struct game *gme);

/**
 * Start running the game.
 *
 * @param game  The game to run
 */
void game_run(struct game *gme);

#endif

