# Laboratoires INF3135 Session Automne 2018

[Sujets des laboratoires](https://gitlab.com/Morriar/inf3135-laboratoires)

### Environnement Linux

* Installation d'une machine virtuelle Linux avec [VirtualBox](https://www.youtube.com/watch?v=1zfO-Fhqyb8)
* Installation du bash subsystem sur Windows [Voir ce lien](https://docs.microsoft.com/en-us/windows/wsl/install-win10)
* Choisissez une distribution basée sur **Debian** tel que
  * [Ubuntu](https://www.ubuntu.com/download)
  * [Mint](https://linuxmint.com/download.php)
  * [Debian](https://www.debian.org/CD/)

#### Quelques fonctions **Unix** utilisées
* `$  cd` -> Change Directory
  * `$  cd ..` -> Revient au dossier parent
* `$  mkdir` -> Make Directory
* `$  ls` -> List Directory
  * `$  ls -l` -> List directory version longue
  * `$  ls -a` -> List directory incluant les fichiers cachés
* `$  rm file` -> Supprime file
  * `$  rm -r folder` -> Supprime folder récursivement
* `$  rmdir folder` -> Supprime folder (si vide)
* `$  cat file` -> Affiche le contenu de file dans la console
* `$  touch file` -> Créé file (vide)
* `$  vim file` -> Ouvre (ou créé) file dans vim

#### Quelques fonctions **git** utilisées
* `$ git init` -> Créer un nouveau projet
* `$ git clone` -> Cloner un projet existant
* `$ git status` -> Liste les nouveaux fichiers et les fichiers modifiés
* `$ git add` -> Versionner un nouveau fichier, Ajouter un fichier pour le prochain commit
* `$ git commit` -> Sauvegarder l’état courant du projet
  * `$ git commit -m "[message descriptif]"`
* `$ git log` -> Consulter l’historique
* `git pull` -> Récupérer des changements à distance
* `git push` -> Téléverser des changements à distance


#### Ressources

* [Cheatsheet pour les commandes **UNIX**](http://cheatsheetworld.com/programming/unix-linux-cheat-sheet/)
* [Cheatsheet pour le format **Markdown**](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
* [Dillinger editeur format **Markdown**](https://dillinger.io/)
* [String to Number Conversion in C Takes its Toll.](http://rus.har.mn/blog/2014-05-19/strtol-error-checking/)
