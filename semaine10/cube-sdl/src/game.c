#include "game.h"
#include <stdbool.h>
#include "application.h"
#include "point.h"

#define X_MOVEMENT 7
#define Y_MOVEMENT 7
#define CUBE_SIZE 10

struct point validate_position(struct point position, struct point move);

struct game *game_initialize(SDL_Renderer *renderer) {
        struct game *gme;
        gme = (struct game*)malloc(sizeof(struct game));
        gme->renderer = renderer;
        gme->state = GAME_PLAY;
        return gme;
}

void game_delete(struct game *gme) {
        if (gme != NULL) {
                free(gme);
        }
}

void game_run(struct game *gme) {

        bool right_key_down = false;
        bool left_key_down = false;
        bool up_key_down = false;
        bool down_key_down = false;

	struct point move;
	struct point position;

	position.x = 0;
	position.y = 0;

        SDL_Event e;
        gme->state = GAME_PLAY;
        while (gme->state == GAME_PLAY) {

		point_set_to_zero(&move);

                while (SDL_PollEvent(&e) != 0) {
                        if (e.type == SDL_QUIT) {
                                gme->state = GAME_QUIT;
                        }  else if (e.type == SDL_KEYDOWN) {
                                if (e.key.keysym.sym == SDLK_LEFT) {
                                        left_key_down = true;
                                }
                    	        if (e.key.keysym.sym == SDLK_RIGHT) {
                                        right_key_down = true;
                                }
                    		if (e.key.keysym.sym == SDLK_UP) {
                                        up_key_down = true;
                                }
                                if (e.key.keysym.sym == SDLK_DOWN) {
                                        down_key_down = true;
                                }

                	}   else if (e.type == SDL_KEYUP) {
                                if (e.key.keysym.sym == SDLK_LEFT) {
                                        left_key_down = false;
                                }
                    	        if (e.key.keysym.sym == SDLK_RIGHT) {
                                        right_key_down = false;
                                }
                    		if (e.key.keysym.sym == SDLK_UP) {
                                        up_key_down = false;
                                }
                                if (e.key.keysym.sym == SDLK_DOWN) {
                                        down_key_down = false;
                                }
                	}
		}

                if (left_key_down) {
                        move.x -= X_MOVEMENT;
                }
                if (right_key_down) {
                        move.x += X_MOVEMENT;
                }
                if (up_key_down) {
                        move.y -= Y_MOVEMENT;
                }
		if (down_key_down) {
			move.y += Y_MOVEMENT;
		}

		position = validate_position(position, move);

                SDL_SetRenderDrawColor(gme->renderer, 0x00, 0x00, 0x00, 0x00 );
                SDL_RenderClear(gme->renderer);

                SDL_Rect fillRect = { position.x, position.y, CUBE_SIZE, CUBE_SIZE};
                SDL_SetRenderDrawColor(gme->renderer, 0xFF, 0x00, 0x00, 0xFF );
                SDL_RenderFillRect(gme->renderer, &fillRect );

		SDL_RenderPresent(gme->renderer);
        }
}

struct point validate_position(struct point position, struct point move) {
	if (position.x + move.x < 0) {
		position.x = 0;
	} else if (position.x + move.x > SCREEN_WIDTH - CUBE_SIZE) {
		position.x = SCREEN_WIDTH - CUBE_SIZE;
	} else {
		position.x += move.x;
	}
        if (position.y + move.y < 0) {
                position.y = 0;
        } else if (position.y + move.y > SCREEN_HEIGHT - CUBE_SIZE) {
                position.y = SCREEN_HEIGHT - CUBE_SIZE;
        } else {
                position.y += move.y;
        }

	return position;
}

