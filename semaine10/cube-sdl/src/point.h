/**
 * File   point.h
 *
 * Defines services from the point module.
 *
 * @author MLasalle
 */

#ifndef POINT_H
#define POINT_H

// --------------- //
// Data structure  //
// --------------- //

struct point {
    int x;
    int y;
};

// --------- //
// Functions //
// --------- //

/**
 * Add one point to another
 *
 * @param t	The target point
 * @param s	The point we are adding to target
 */
void point_add(struct point *t, const struct point *s);

/**
 * Subtract one point from another
 *
 * @param t      The target point
 * @param s      The point we are subtracting from target
 */
void point_subtract(struct point *t, const struct point *s);
/**
 * Set point to a value
 *
 * @param t      The target vector
 * @param x      The x value
 * @param y	The y value
 */
void point_set_to(struct point *t, int x, int y);
/**
 * Set vector x an y to zero
 *
 * @param t 	The target vector
 */
void point_set_to_zero(struct point *t);
#endif /* VECTOR_H */

