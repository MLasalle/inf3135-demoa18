/**
 * @origin https://bitbucket.org/ablondin-projects/maze-sdl
 */
#include "application.h"
#include "sdl2.h"
#include <stdio.h>

int main()
{
        struct application *app = application_initialize();
        if (app != NULL) {
                application_run(app);
        } else {
                fprintf(stderr, "Failed to initialize the application...");
                return -1;
        }
        application_shut_down(app);
}
