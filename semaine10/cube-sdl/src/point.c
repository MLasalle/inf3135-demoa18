/**
 * File point.c
 *
 * Implements services provided by the point module
 *
 * @author Maxime Phaneuf
 */
#include "point.h"

void point_add(struct point *t, const struct point *s)
{
    t->x += s->x;
    t->y += s->y;
}

void point_subtract(struct point *t, const struct point *s)
{
    t->x -= s->x;
    t->y -= s->y;
}

void point_set_to(struct point *t, int x, int y)
{
    t->x = x;
    t->y = y;
}

void point_set_to_zero(struct point *t)
{
     point_set_to(t, 0, 0);
}
