/**
 * File Application.h
 *
 * Define services provided by application module.
 *
 * @origin https://bitbucket.org/ablondin-projects/maze-sdl
 */

#ifndef APPLICATION_H
#define APPLICATION_H

#include "sdl2.h"
#include "game.h"

#define SCREEN_WIDTH  800
#define SCREEN_HEIGHT 600

// --------------- //
// Data structures //
// --------------- //

enum application_state {
        APPLICATION_STATE_PLAY, // We are playing
        APPLICATION_STATE_QUIT  // We are quitting
};

struct application {
        enum application_state state; // The current state
        SDL_Window* window;          // The window
        SDL_Surface* surface;        // The surface
        SDL_Renderer* renderer;      // The renderer
	struct game *gme;
};

// --------- //
// Functions //
// --------- //

/**
 * Creates a new application.
 *
 * @return  A pointer to a new application, NULL if there was an error
 */
struct application *application_initialize();

/**
 * Start running the application.
 *
 * @param application  The application to run
 */
void application_run(struct application *app);

/**
 * Closes the given application.
 *
 * @param application  The application to be closed
 */
void application_shut_down(struct application *app);

#endif
