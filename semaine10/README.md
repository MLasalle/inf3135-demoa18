# Laboratoire 10 | 27 novembre 2018

Tp3

## Resources

- [Jeu Maze](https://bitbucket.org/ablondin-projects/maze-sdl) pour une structure de base d'applications graphiques.
- [SDL/TMX exemple](https://github.com/baylej/tmx/blob/master/examples/sdl/sdl.c) pour afficher l'arrière plan créé avec Tiled.
- [Lazy Foo tutoriel SDL](http://lazyfoo.net/tutorials/SDL/).
- [Open Game art](https://opengameart.org/).
- [Linux kernel coding style](https://www.kernel.org/doc/html/v4.10/process/coding-style.html).


## Dépendances SDL
 
- [SDL2](https://www.libsdl.org/)
- [SDL image](https://www.libsdl.org/projects/SDL_image/)
- [SDL ttf](https://www.libsdl.org/projects/SDL_ttf/)

## Dépendances autres

- [Libxml2](http://xmlsoft.org/)
- [TMX parser](https://github.com/baylej/tmx)
- [CMake](https://cmake.org/)